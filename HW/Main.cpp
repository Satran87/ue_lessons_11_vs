#include <iostream>
#include <immintrin.h>
#include <string>
#include "CPUID.h"
using namespace std;
inline void print(string_view MyText)
{
	cout << MyText << endl;
}
int main()
{
	const string baseRandText = "base Rand()";
	unsigned randNum;
	string UsingText("Using ");
	if (InstructionSet::RDRAND())
	{
		if (_rdrand32_step(&randNum) == 0)
		{
			UsingText += baseRandText;
			randNum = rand();
		}
		else
			UsingText += "Intel Rand()";
	}
	else
	{
		UsingText += baseRandText;
		randNum = rand();
	}
		
	print(UsingText);
	print(to_string(randNum));
	system("pause");
	return EXIT_SUCCESS;
}